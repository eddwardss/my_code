a = list(range(1, 101))
for i in a:
    if i%15 == 0: print(i, 'FuzzBuzz')
    if i%3 == 0 and i%15!=0: print(i, 'Fuzz')
    if i%5 == 0 and i%15!=0: print(i, 'Buzz')
