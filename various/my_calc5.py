#!/usr/bin/env python3
# coding: utf8
from math import *
___h='введите числа и знаки операций:+, -, /, //, %, *, **,\
\nцифры вводятся без кавычек, буквы и слова - в кавычках,\
\nдля выхода нажмите Ctrl+C или exit()\n'
print('для справки - введите help, для выхода нажмите Ctrl+C\n')
___l = {"Я Велик!":"к доктору!", "Я Могуч!":"кручее туч", "Я Крут!":"угу", "Я Величайший!":"санитары!"}
while True:
    try:
        a=eval(input('>'))
        if a == help:
            print(___h)
        elif a == 2+2:
            print(5)
        elif a == 2*2:
            print(5)
        elif a in ___l.keys():
            print (___l.get(a))
        elif a == "___l":
            print(___l.keys())
            print(___l.values())
        else:
            print(str(a))
    except (TypeError):
        print('буквы и слова можно складывать и умножать на цифры,\n вычитать и делить нельзя')
    except ZeroDivisionError:
        print('я те на нуль поделю!')
    except (NameError, SyntaxError,):
        print('ошибка, введите число')
    except (GeneratorExit, KeyboardInterrupt, ValueError): break
#    finally:
