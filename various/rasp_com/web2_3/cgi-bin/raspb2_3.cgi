#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

import cgi
import time
import sys
import __future__
import RPIO
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD) #"включение" GPIO
GPIO.setup(3, GPIO.OUT)  #объявление 3-го пина как выход
GPIO.setup(5, GPIO.OUT)
GPIO.setup(7, GPIO.OUT)
GPIO.setup(8, GPIO.OUT)
GPIO.setup(10, GPIO.OUT)
rbuttonsA = []
rbuttonsC = []
rbuttonsK = []
rbuttons0 = []

# Create instance of FieldStorage
form = cgi.FieldStorage()

if sys.version[:1] == '3':
    if form.getvalue('rbuttonsA'):
        rbuttonsA = form.getvalue('rbuttonsA')
        if rbuttonsA == "потеря связи корпус А":
            GPIO.output(3, True)  # или GPIO.output(3, 1)
            rbuttonsA += ' - Сделано'
        else:
            GPIO.output(3, False)
            rbuttonsA += ' - Выполнено'
    if form.getvalue('rbuttonsC'):
        rbuttonsC = form.getvalue('rbuttonsC')
        if rbuttonsC == "потеря связи корпус C":
            GPIO.output(5, True)
            rbuttonsC += ' - Сделано'
        else:
            GPIO.output(5, False)
            rbuttonsC += ' - Выполнено'
    if form.getvalue('rbuttonsK'):
        rbuttonsK = form.getvalue('rbuttonsK')
        if rbuttonsK == "бачок открыт":
            GPIO.output(7, True)
            rbuttonsK += ' - Сделано'
        else:
            GPIO.output(7, False)
            rbuttonsK += ' - Выполнено'
    if form.getvalue('rbuttons0'):
        rbuttons0 = form.getvalue('rbuttons0')
        if rbuttons0 == "возвращено в исходное состояние":
            time.sleep(3)        # задержка 3 сек
            GPIO.cleanup()
            rbuttons0 += ' - Сделано'
        else:
            rbuttons0 += ' - Нет'
else:
    if form.getvalue('rbuttonsA'):
        rbuttonsA = form.getvalue('rbuttonsA')
        if rbuttonsA == "потеря связи корпус А":
            GPIO.output(3, True)  # или GPIO.output(3, 1)
            rbuttonsA += ' - Сделано'
        else:
            GPIO.output(3, False)
            rbuttonsA += ' - Выполнено'
    if form.getvalue('rbuttonsC'):
        rbuttonsC = form.getvalue('rbuttonsC')
        if rbuttonsC == "потеря связи корпус C":
            GPIO.output(5, True)
            rbuttonsC += ' - Сделано'
        else:
            GPIO.output(5, False)
            rbuttonsC += ' - Выполнено'
    if form.getvalue('rbuttonsK'):
        rbuttonsK = form.getvalue('rbuttonsK')
        if rbuttonsK == "бачок открыт":
            GPIO.output(7, True)
            rbuttonsK += ' - Сделано'
        else:
            GPIO.output(7, False)
            rbuttonsK += ' - Выполнено'
    if form.getvalue('rbuttons0'):
        rbuttons0 = form.getvalue('rbuttons0')
        if rbuttons0 == "возвращено в исходное состояние":
            time.sleep(3)        # задержка 3 сек
            GPIO.cleanup()
            rbuttons0 += ' - Сделано'
        else:
            rbuttons0 += ' - Нет'

print("Content-type:text/html\r\n\r\n")
print("<html>")
print("<head>")
print('<meta charset="utf-8">')
print("<title>Состояние</title>")
print("</head>")
print("<body>")
print("<h2> Состояние:&nbsp;&nbsp;%s</h2>" % rbuttonsA)
print("<h2> Состояние:&nbsp;&nbsp;%s</h2>" % rbuttonsC)
print("<h2> Состояние:&nbsp;&nbsp;%s</h2>" % rbuttonsK)
print("<h2> Состояние:&nbsp;&nbsp;%s</h2>" % rbuttons0)
print("</body>")
print("</html>")
