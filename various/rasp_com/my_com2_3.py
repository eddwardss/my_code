#!/usr/bin/env python
# coding: utf8
import time
import sys
import __future__
import RPIO
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD) #"включение" GPIO
GPIO.setup(3, GPIO.OUT)  #объявление 3-го пина как выход
GPIO.setup(5, GPIO.OUT)
GPIO.setup(7, GPIO.OUT)
GPIO.setup(8, GPIO.OUT)
GPIO.setup(10, GPIO.OUT)
___h = 'выберите действия\
\n1 - потеря связи корпус А\
\n2 - восстановить корпус А\
\n3 - потеря связи корпус С\
\n4 - восстановить корпус С\
\n5 - открыть бачок\
\n6 - закрыть бачок\
\n7 - вернуть в исходное состояние\
\nдля справки - введите help\
\nдля выхода введите exit\n'
print(___h)
while True:
    try:
        if sys.version[:1] == '3':
            a = input('введите выбранное >')
            if a == help:
                print(___h)
            elif a == '1':
                print("потеря связи корпус А")
                GPIO.output(3, True)  # или GPIO.output(3, 1)
            elif a == '2':
                print("восстановить корпус А")
                GPIO.output(3, False)
            elif a == '3':
                print("потеря связи корпус C")
                GPIO.output(5, True)
            elif a == '4':
                print("восстановить корпус C")
                GPIO.output(5, False)
            elif a == '5':
                print("бачок открыт")
                GPIO.output(7, True)
            elif a == '6':
                print("бачок закрыт")
                GPIO.output(7, False)
            elif a == '7':
                time.sleep(2)        # задержка 2 сек
                GPIO.cleanup()
                print("возвращено в исходное состояние")
            elif a == 'help':
                print(___h)
            elif a == "exit":
                time.sleep(2)        # задержка 2 сек
                print("гуд бай!")
                break
            else:
                print(str(a))
        else:
            a = input('введите выбранное >')
            if a == help:
                print(___h)
            elif a == 1:
                print("потеря связи корпус А")
                GPIO.output(3, True)  # или GPIO.output(3, 1)
            elif a == 2:
                print("восстановить корпус А")
                GPIO.output(3, False)
            elif a == 3:
                print("потеря связи корпус C")
                GPIO.output(5, True)
            elif a == 4:
                print("восстановить корпус C")
                GPIO.output(5, False)
            elif a == 5:
                print("бачок открыт")
                GPIO.output(7, True)
            elif a == 6:
                print("бачок закрыт")
                GPIO.output(7, False)
            elif a == 7:
                time.sleep(2)        # задержка 2 сек
                GPIO.cleanup()
                print("возвращено в исходное состояние")
            elif a == 'help':
                print(___h)
            elif a == exit:
                time.sleep(2)        # задержка 2 сек
                print("гуд бай!")
                break
            else:
                print(str(a))
    except (TypeError):
        print('ошибка, введите число')
    except ZeroDivisionError:
        print('я те на нуль поделю!')
    except (NameError, SyntaxError,):
        print('ошибка, введите число')
    except (GeneratorExit, KeyboardInterrupt, ValueError): 
        break
